# Satisfactory saveGame Editor

**Auf der Seite [Releases](https://github.com/Goz3rr/SatisfactorySaveEditor/releases) können Sie eine kompilierte Veröffentlichung herunterladen** oder Ihre eigene erstellen, indem Sie dieses Repo klonen.
Stucks? Bugs? Fragen? Kontaktieren Sie uns hier auf Bitbucket oder auf dem Discord-Server [Satisfactory Modding](https://discord.gg/rNxYXht).

Ein in der Entwicklung befindlicher Save-Game Editor für Satisfactory. Besteht sowohl aus einem Parser zum Speichern als auch aus einer Anwendung zum Anzeigen und Bearbeiten der analysierten Daten.

Features sind:
* Alle gespeicherten Daten in einem besser lesbaren Format anzeigen
* Bearbeiten Sie Daten-Tags manuell
* Aktivieren Sie die Minikarte
* Ändern Sie Ihre Inventargröße
* Schalte alle Meilensteine ​​frei, auch unveröffentlichte
* Löschen Sie Dinge von Ihrem Speicher
* Mehr folgt bald

Geschrieben in C # mit WPF für die Benutzeroberfläche.

Hergestellt in Visual Studio 2019. Erfordert .NET Framework 4.7.2 Dev Pack und .NET Core 2.2 SDK.

# Hilfe

Wenn Sie mit der Maus über Elemente in der Strukturansicht des linken Bereichs fahren, erhalten Sie eine kurze Beschreibung ihres Zwecks beim Speichern. Bitte helfen Sie uns, diese Liste auf dem neuesten Stand zu halten, indem Sie [`Types.xml`](https://github.com/Goz3rr/SatisfactorySaveEditor/blob/master/SatisfactorySaveEditor/Types.xml) mit den Tags aktualisieren, die Sie in Ihrer Sicherungsbearbeitung finden Abenteuer.

Wenn Sie Fragen haben, können Sie uns gerne hier auf Bitbucket oder auf Discord kontaktieren.

Auf unserem Discord-Server [Satisfactory Modding](https://discord.gg/rNxYXht) können Sie im channel `# savegame-edits` Hilfe anfordern.

# FAQ

* Keine der Änderungen, die ich vornehme, hat etwas mit dem Spiel zu tun.

Sind Sie sicher, dass Sie "Datei> Speichern" zum Speichern verwenden, nachdem Sie Ihre Änderungen vorgenommen haben?

* Ich versuche den Cheat "Alle Forschungen freischalten" zu benutzen, aber er funktioniert nicht!

Einige Leute sind auf Probleme gestoßen, wenn sie die HUB-Stufe 0 nicht abschließen, bevor sie diesen Cheat ausführen. Versuchen Sie das und kontaktieren Sie uns, wenn das Problem weiterhin besteht.

* Das Platzieren einiger Dinge, die ich mit dem Cheat "Alle Forschungen freischalten" freigeschaltet habe, stürzt mein Spiel ab!

Vor ein paar Updates wurde etwas im Spiel geändert, um Abstürze zu verursachen, wenn einige, aber nicht alle unveröffentlichten Gebäude platziert werden. Wir sind uns nicht sicher, ob dies absichtlich von Coffee Stain gemacht wurde oder ob es zufällig passiert ist, aber wir können nichts dagegen tun.